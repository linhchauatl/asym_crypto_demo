require './encryption_util'

original_text = 'Here is a plain text.'
puts "Original text = #{original_text}"

EncryptionUtil.load_keys('./public_key.pem', './private_key.pem')

# Use public key to encrypt, and use private key to decrypt
puts("\n\n1 === Use public key to encrypt, and use private key to decrypt ===\n\n")
public_enc  = EncryptionUtil.public_encrypt(original_text)
private_dec = EncryptionUtil.private_decrypt(public_enc)

puts("public_enc (Ciphertext) =\n#{public_enc}\nprivate_dec (Plain text) = #{private_dec}\n\n\n")

# Use private key to encrypt, and use public key to decrypt
puts("\n\n2 === Use private key to encrypt, and use public key to decrypt ===\n\n")
private_enc = EncryptionUtil.private_encrypt(original_text)
public_dec  = EncryptionUtil.public_decrypt(private_enc)

puts("private_enc (Ciphertext) =\n#{private_enc}\npublic_dec (Plain text ) = #{public_dec}\n\n\n")
