## Asymmetric Encryption / Decryption Demo

This program demonstrates the use of public / private keypair to encrypt and decrypt data.

**1. Use public key to encrypt, and use private key to decrypt**

Use case: Secret data exchange. Person A wants person B to send information to him, that only person A can read. Person A give a public key to person B. Each time person B wants to send information to person A, he uses the public key to encrypt data, creates an encrypted text (ciphertext), then ***send only the ciphertext*** to person A. Person A uses private key to decrypt the ciphertext to read the information.

In this case, anybody who has person A's public key can create secret information to send to the person A. But A is the only person  who has private key, and only A can decrypt and read the information.

**2. Use private key to encrypt, and use public key to decrypt**

Use case: Signature and data integrity validation. Person A wants to assure person B that the information is really sent from A, and the information is not tampered with. Person A uses private key to encrypt (sign) the information (plain text), and ***send encrypted text (in this case it is called signature) AND the information*** to person B. Person B uses public key to decrypt the signature. If the decrypted text from the signature matches with the content of information, it means that the information is really sent from A (the only person who has private key), and not tampered with. Otherwise, we know that the information is either not sent from person A or intercepted and modified along the way.

In this case, person A is the only person who has the private key, and only him can sign the data (encrypt data) so that the correspondent public key can decrypt it. That is why the ciphertext is called a digital signature.

**Note**: There are other ways to ensure the integrity of data, for example using one-way hash functions such as MD5 or HMAC.  
